import json
import logging
import pickle
import re

from flask_cors import CORS, cross_origin
import numpy as np
import sentencepiece as spm
import torch
import torch.nn as nn
from flask import Flask, request, Response

logger = logging.getLogger(__name__)


def rule1(word):
    rule = r'[aáàảãạăắằẳẵặâấầẩẫậeéèẻẽẹêếềểễệoóòỏõọôốồổỗộơớờởỡợiíìỉĩịuúùủũụưứừửữựyýỳỷỹỵ][hg]'
    error = re.findall(rule, word)
    if len(error) != 0:
        return word.replace(error[0], 'n'.join(error[0]))
    else:
        return word


def rule2(word):
    def rule_(word, character):
        rule = character + '{2,}'
        error = re.findall(rule, word)
        if len(error) != 0:
            return word.replace(error[0], character)
        else:
            return word

    characters = 'qwrtyipsdfghjklzxcvbnmaáàảãạăắằẳẵặâấầẩẫậeéèẻẽẹêếềểễệóòỏõọoôốồổỗộơớờởỡợíìỉĩịúùủũụưứừửữựýỳỷỹỵ'
    n = len(characters)
    for i in range(n):
        word = rule_(word, characters[i])
    return word


def rule3(word):
    def rule_(word, character):
        rule = character
        error = re.findall(rule, word)
        if len(error) != 0:
            return word.replace(error[0], character[1] + character[0])
        else:
            return word

    errors = ['hn', 'hc', 'ig', 'hg', 'hk', 'gn', 'ht', 'rt', 'uq', 'hp', 'êi', 'êy', 'ôu', 'ơư', 'ei', 'ey', 'ou',
              'oư', 'ơu', 'aư', 'âư']
    for error in errors:
        word = rule_(word, error)
    return word


def rule4(word):
    def rule_(word, dict_error):
        dict_error = dict(dict_error)
        rules = list(dict_error.keys())
        sol = list(dict_error.values())
        for i in range(len(rules)):
            error = re.findall(rules[i], word)
            if len(error) != 0:
                word = word.replace(error[0], sol[i])
        return word

    errors = [{'uu': 'ưu'}, {'uo': 'ươ', 'ưo': 'ươ', 'uơ': 'ươ'}, {'ie': 'iê'}, {'ye', 'yê'}]
    for error in errors:
        word = rule_(word, error)
    return word


def rule5(word):
    def rule_(word, dict_error):
        dict_error = dict(dict_error)
        rules = list(dict_error.keys())
        sol = list(dict_error.values())
        for i in range(len(rules)):
            error = re.findall(rules[i], word)
            if len(error) != 0:
                word = word.replace(error[0], sol[i])
        return word

    errors = [{'ưy': 'ưi'}, {'ym': 'im'}]
    for error in errors:
        word = rule_(word, error)
    return word


# import VietnameseTextNormalizer
#
#
# def rule6(string: str):
#     # # example
#     # print('hiếu thảo' == 'hiếu thảo')
#     # print(VietnameseTextNormalizer.Normalize('hiếu thảo') == 'hiếu thảo')
#     # print(VietnameseTextNormalizer.Normalize('hoà thuận'))
#     return VietnameseTextNormalizer.Normalize(string).replace('\n', '. ')


with open('teencode.txt') as f:
    content_teencode = f.readlines()
    teencode = {}
    for line in content_teencode:
        tmp = line.replace("\n", "").split("\t")
        teencode[tmp[0]] = tmp[1]


def rule_temp(word):
    return word.replace('oo', 'ô').replace('aa', 'â').replace('ee', 'ê')


def rule12(word: str):
    if teencode.get(word, None) is None:
        return word
    return teencode[word]


def rule8(string: str):
    acronym = {
        'f': 'ph'
    }
    for word in acronym.keys():
        if string.startswith(word):
            mapping = acronym.get(word)
            return string.replace(word, mapping)
    return string


punctuation_end = ['.', '!', '?']
character_before_sentence = [' ', '\n']


def rule13(text: str):
    i = 0
    while i < len(text):
        if text[i] in punctuation_end:
            if text[i + 1] in character_before_sentence:
                text = text[:i + 2] + text[i + 2].upper() + text[i + 3:]
            else:
                text = text[:i + 1] + " " + text[i + 1:]
                i = i - 2
        i += 1
    return text


class Test:
    def __init__(self,lib,one_grams,bi_grams,tri_grams):
        """
        input: 
            lib: tu dien cac tu don o dang dict
            n_grams: sac xuat n_grams luu o dang dict
        """
        self.lib = lib
        self.one_grams=one_grams
        self.bi_grams=bi_grams
        self.tri_grams=tri_grams
        self.lib_word=list(self.lib.keys())
        
    def check_charactor(self,word):
        """
        input: 1 tu
        output: return true neu tu do khong phai la ky tu dac biet va nguoc lai
        """
        match=re.findall(r'[a-zA-Z0-9]+',word)
        if(len(match)==0):
            return False
        return True
        
    def editDt(self,word1,word2):
        """
        input: 2 tu
        ouput: khoang cach edit distences
        """
        m=len(word1)
        n=len(word2)
        dp=np.zeros((m+1,n+1))

        for i in range(m + 1):
            for j in range(n + 1):
                if i == 0:
                    dp[i][j] = j    # Min. operations = j
 
                elif j == 0:
                    dp[i][j] = i    # Min. operations = i
 
                elif word1[i-1] == word2[j-1]:
                    dp[i][j] = dp[i-1][j-1]
 
                else:
                    dp[i][j] = 1 + min(dp[i][j-1],        # Insert
                                       dp[i-1][j],        # Remove
                                       dp[i-1][j-1])    # Replace
 
        return dp[m][n]
        
    def rules(self,word):
        """
        input: 1 tu khong co trong tu dien
        ouput: 1 tu sau khi da cho qua cac tap luat
        """
        def check(word):
            try: 
                tmp=self.lib[word]
                return True
            except:
                return False
        def rule1(word):
            rule=r'[aáàảãạăắằẳẵặâấầẩẫậeéèẻẽẹêếềểễệoóòỏõọôốồổỗộơớờởỡợiíìỉĩịuúùủũụưứừửữựyýỳỷỹỵ][hg]'
            error=re.findall(rule,word)
            if(len(error)!=0):
                return word.replace(error[0],'n'.join(error[0]))
            else:
                return word
        def rule2(word):
            charactor = r'jf'
            for c in charactor:
                error=re.findall(c,word)
                if(len(error)!=0):
                    word=word.replace(c,'')
            return word
        
        if check(word) == False:
            word = rule1(word)
#         if check(word) == False:
#             word = rule2(word)
        return word            
        
    def get_candidate(self,word,threshold):
        """
        input: 1 tu X
        output: danh sach candidate voi tu X dung dau: VD: X, can_1, can_2,... 
        """
        candidate=[]
        for i in range(len(self.lib_word)):
            word2=self.lib_word[i]
            dis=self.editDt(word,word2)
            if(dis<=threshold and dis!=0):
                candidate.append(self.lib_word[i])
        return [word]+candidate
    
    def compute_prob(self,sentence,n_gram,size_ngrams):
        list_word=sentence.split()
        n_sent=[]
        for i in range(len(list_word)-size_ngrams+1):
            n_sent.append(' '.join(list_word[i:i+size_ngrams]))
        prob=0
        for i in range(len(n_sent)):
            try:
                prob+=n_gram[n_sent[i]]
            except:
                pass
#         if prob/len(n_sent) > 0:
#             print(n_sent)
#             print(prob/len(n_sent))
        return prob/len(n_sent)
    
    def predict_Hoang(self,sentence,list_word_error):
        """
        input: 
            sentence: 1 cau goc
            list_word_error: list cac tu sai
        output: dict co dang : {idx_tu_sai:[candidate_1,candidate_2,.....],....}
        """
        
        sentence=sentence.lower()
        #sentence=re.sub(r'[!~`@#$%^&*\(\)\-\+\'\":;,.<>\/\\?]+','',sentence)
        words=sentence.split()
        if(len(words)==1):
            n_grams=self.one_grams
            size_ngrams=1
        elif len(words) == 2:
            n_grams=self.bi_grams
            size_ngrams=2
        else:
            n_grams=self.tri_grams
            size_ngrams=3
            
        # cho cac tu sai qua rules
        for i in list_word_error:
            words[i]=self.rules(words[i])
        
        list_word=[] # chua danh sach cac tu trong cau goc
        
        for i in range(len(words)):
            if(self.check_charactor(words[i])):
                list_word.append(i)
                
        idx_word_error_new=[list_word.index(x) for x in list_word_error]
        words_clear=[words[x] for x in list_word]
        fix={}
        for i in idx_word_error_new:
            candidates=self.get_candidate(words_clear[i],threshold=2)
#             print(candidates)
            list_pro=[]
            for can in candidates:
                new_sentence=' '.join(words_clear[max(0,i-size_ngrams+1):i]+[can]+words_clear[i+1:i+size_ngrams])
                prob=self.compute_prob(new_sentence,n_grams,size_ngrams)
                list_pro.append(prob)
#        
            idx_sort=np.argsort(-np.array(list_pro[1:]))+1
            fix.update({list_word[i]:np.array(candidates)[idx_sort[:10]]})
        return fix
        
    def predict_Loc(self,sentence):
        def check_max(list_prob):
            max_=list_prob[0]
            idx=0
            for i in range(1,len(list_prob)):
                if(list_prob[i]>max_):
                    max_=list_prob[i]
                    idx=i
            return idx
        
        sentence=sentence.lower()
        words=sentence.split()
        list_word=[] # chua danh sach index cua cac tu trong cau goc
        
        for i in range(len(words)):
            if(self.check_charactor(words[i])):
                list_word.append(i)
        words_clear=[words[x] for x in list_word]  # list cac tu trong cau goc
        for i in range(len(words_clear)):
            words_clear[i]=re.sub(r'[!~`@#$%^&*\(\)\-\+\'\":;,.<>\/\\?]+','',words_clear[i])
            
        if(len(words_clear)==1):
            n_grams=self.one_grams
            size_ngrams=1
        elif len(words_clear) == 2:
            n_grams=self.bi_grams
            size_ngrams=2
        else:
            n_grams=self.tri_grams
            size_ngrams=3
            
        # Cho tat ca cac tu qua rules ( trong rules se kiem tra tu co trong tu dien hay khong)
        for i in range(len(words_clear)):
            words_clear[i]= self.rules(words_clear[i])
        
        fix={} # dict co dang : {index_tu_sai: tu_dung_predict,....}
        for i in range(len(words_clear)):
            candidates=self.get_candidate(words_clear[i],threshold=2)
#             print(candidates)
            list_pro=[]
            for can in candidates:
                new_sentence=' '.join(words_clear[max(0,i-size_ngrams+1):i]+[can]+words_clear[i+1:i+size_ngrams])
#                 print(new_sentence)
                prob=self.compute_prob(new_sentence,n_grams,size_ngrams)
                list_pro.append(prob)
#             print(list_pro)
            idx_prob_max=check_max(list_pro)
#             print(idx_prob_max)
            if(idx_prob_max!=0):
                idx_sort=np.argsort(-np.array(list_pro[1:]))+1
                fix.update({list_word[i]:np.array(candidates)[idx_sort[:10]]})
                # sua lai tu trong cau luon
                words_clear[i]= candidates[idx_prob_max]
        return fix
                
        
    def score(self,sentences, predict):
        """
        input:
            sentences: cau goc da duoc loai bo ky tu d
        """


with open('test_v1/pro_trigrams_v1.pkl','rb') as f:
    tri_grams=pickle.load(f)
with open('test_v1/pro_bigrams_v1.pkl','rb') as f:
    bi_grams=pickle.load(f)
with open('test_v1/pro_unigrams_v1.pkl','rb') as f:
    one_grams=pickle.load(f)
with open('test/lib_word.pkl','rb') as f:
    lib=pickle.load(f)

test = Test(lib, one_grams,bi_grams, tri_grams)

def num_parameters(parameters):
    num = 0
    for i in parameters:
        num += len(i)
    return num


class Detector(nn.Module):
    def __init__(self, input_dim, output_dim, embedding_dim, num_layers, hidden_size):
        super(Detector, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.embedding_dim = embedding_dim
        self.num_layers = num_layers
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(num_embeddings=self.input_dim, embedding_dim=self.embedding_dim, )
        self.LSTM = nn.LSTM(input_size=self.embedding_dim, hidden_size=self.hidden_size, num_layers=self.num_layers,
                            batch_first=True, dropout=0.1, bidirectional=True)
        self.linear = nn.Linear(self.hidden_size * 2, self.output_dim)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        emb = self.embedding(x)
        outputs, (h_n, h_c) = self.LSTM(emb)
        logits = self.linear(outputs)

        p = self.sigmoid(logits)
        return p


class HardMasked(nn.Module):
    def __init__(self, detector, detector_tokenizer, device):
        super(HardMasked, self).__init__()

        self.detector = detector.to(device)
        self.detector_tokenizer = detector_tokenizer
        self.use_device = device

    def forward(self, s):
        maskedlm_features = self.prepare_input(s)
        return maskedlm_features

    def prepare_input(self, s):

        detector_input_ids = self.detector_tokenizer.encode(s, out_type=int)
        # print(detector_input_ids)
        detector_input_pieces = self.detector_tokenizer.id_to_piece(detector_input_ids)
        detector_outputs = (self.detector(torch.tensor([detector_input_ids], dtype=torch.long, device=self.use_device))[
                                0].reshape(1, -1) > 0.5).int()[0]
        # print(detector_input_pieces)
        # print(detector_outputs)
        index = []
        for i in range(0, len(detector_input_pieces)):
            if detector_outputs[i] == 1:
                index.append(i)
                # detector_input_pieces[i] = ' <mask>'
        count = 0
        for i in range(len(detector_input_pieces)):
            if detector_input_pieces[i][0] != "▁":
                for j in range(len(index)):
                    if index[j] >= i - count:
                        index[j] = index[j] - 1
                count = count + 1
        index = set(index)
        return index


detector_path = 'Detector_old.pkl'
detector_tokenizer_path = 'spm_tokenizer10000.model'

device = 'cuda' if torch.cuda.is_available() else 'cpu'
detector_tokenizer = spm.SentencePieceProcessor(detector_tokenizer_path)

detector = torch.load(detector_path, map_location=torch.device('cpu'))

model = HardMasked(detector, detector_tokenizer, device)

s = 'Một ngày noj em đi tham oong bà. Hôm qua em ddi choi'

def check_charactor(word):
    """
    input: 1 tu
    output: return true neu tu do khong phai la ky tu dac biet va nguoc lai
    """
    match=re.findall(r'[a-zA-Z0-9]+',word)
    if(len(match)==0):
        return False
    return True   

def spell_correction(input_s: str):
    tu_sai = []
    count = 0
    id = 0
    s_new = ""

    for a in input_s.strip().split("."):
        if(a==""):
            continue
        print(a)
        a=a.strip()
        words=a.split()
        for i in range(len(words)):
            if check_charactor(words[i]):
                words[i]=re.sub(r'[!~`@#$%^&*\(\)\-\+\'\":;,.<>\/\\?\”\“]+','',words[i])
        a= ' '.join(words)
        a=a.lower()
        vitrisai=model(a)  # vi tri cua tu sai trong cau
        vitrisai = list(vitrisai)
        print(vitrisai)
        vitrisai.sort()
        mang_cau=a.split(" ")
        mangfix=test.predict_Hoang(a,vitrisai)

        for i in vitrisai:
            id+=1
            dict1={'id':id,'name':mang_cau[i],'cau':count,'vitri':i,'list_tu_sua':mangfix[i].tolist()}
            tu_sai.append(dict1)
        count += 1

    print(tu_sai)
    return Response(json.dumps({'data': tu_sai}),  mimetype='application/json')


app = Flask(__name__)
CORS(app)


@app.route("/", methods=['POST'])
def correct():
    content = request.get_json()
    if content is None:
        return {}
    print(content)
    data = spell_correction(content['sentence'])
    return data


if __name__ == '__main__':
    # print(spell_correction(s))
    app.run(debug=True, host='0.0.0.0', port=50000)
